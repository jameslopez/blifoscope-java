package es.jameslopez.bliffoscope.scanner;

/**
 * Class: AccumulationPointMap
 *
 * Handles a 2D array where each point with location (x,y) will
 * represent the number of points matched for a space item (value), related to
 * the location in the image as [x][y].
 */
public class AccumulationPointMap {

    private int [][] accumulationPointMapArray;

    /**
     * Instantiates a new Accumulation point map.
     *
     * @param accumulationPointMapArray the accumulation point map array
     */
    public AccumulationPointMap( int[][] accumulationPointMapArray) {
        this.accumulationPointMapArray = accumulationPointMapArray.clone();
    }

    /**
     * Get accumulation point map array.
     *
     * @return the int [ ] [ ]
     */
    public int[][] getAccumulationPointMapArray() {
        return accumulationPointMapArray.clone();
    }

    /**
     * Prettifies the output of an accumulation point Map and saves it into a String.
     *
     * @return accumulation point map array String representation.
     */
    @Override
    public String toString(){
        StringBuilder accumulationPointMapString = new StringBuilder();
        for(int row = 0; row < accumulationPointMapArray.length ; row++) {
            for(int col = 0; col < accumulationPointMapArray[row].length; col++) {
                accumulationPointMapString.append(String.format("%15s", "[" + row + "][" + col + "]"
                        + accumulationPointMapArray[row][col]));
            }
            accumulationPointMapString.append(System.getProperty("line.separator"));
        }
        return accumulationPointMapString.toString();
    }
}
