package es.jameslopez.bliffoscope.scanner;

import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;
import es.jameslopez.bliffoscope.spaceitem.Image;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Class: GenericSpaceItemScanner
 */
public class GenericSpaceItemScanner implements SpaceItemScanner {

    /** accumulation point map @see AccumulationPointMap */
    private AccumulationPointMap accumulationPointMap;

    private int spaceItemArea;
    private int probability;
    private int duplicateAreaPoints;

    /**
     * Instantiates a new Generic space item scanner.
     *
     * @param probability the probability
     * @param duplicateAreaPoints the duplicate area points
     */
    public GenericSpaceItemScanner(int probability, int duplicateAreaPoints) {
        this.probability = probability;
        this.duplicateAreaPoints = duplicateAreaPoints;
    }

    /**
     * Finds the space item
     *
     * @param spaceItem the space item
     * @param image the image
     */
    @Override
    public void findSpaceItem(AbstractSpaceItem spaceItem, Image image){

        int [][] accumulationPointMapArray = new int[image.getWidth()][image.getHeight()];

        spaceItemArea = spaceItem.getHeight() * spaceItem.getWidth();

        //Binary images for the space item and image
        boolean[][] antiNeutrinoBinaryImageFromSpaceItem = spaceItem.getAntiNeutrinoBinaryImage();
        boolean[][] antiNeutrinoBinaryImageFromImage = image.getAntiNeutrinoBinaryImage();

        /** Loop through the image and find any space item
         *  - It will generate an accumulation point map array.
         *  i.e
         *  [x][y] = pointNumber;
         *
         *  representing how many points are the same in the image within a space item region.
         *
         *  This is a simple XOR:
         *
         *  Space item: + Image: + -> add point
         *  Space item:   Image:   -> add point
         *
         *  Obviously, distinct points do not get added.
         *
         */
        for(int row = 0; row < antiNeutrinoBinaryImageFromImage.length ; row++) {
            for(int col = 0; col < antiNeutrinoBinaryImageFromImage[row].length; col++) {

                int accumulationPointValue = 0;

                for(int itemRow = 0; itemRow < antiNeutrinoBinaryImageFromSpaceItem.length ; itemRow++) {
                    for(int itemCol = 0; itemCol < antiNeutrinoBinaryImageFromSpaceItem[itemRow].length
                            && (itemRow + row < antiNeutrinoBinaryImageFromImage.length) &&
                            (itemCol + col < antiNeutrinoBinaryImageFromImage[itemRow].length); itemCol++) {

                        if(! antiNeutrinoBinaryImageFromSpaceItem[itemRow][itemCol] ^
                                antiNeutrinoBinaryImageFromImage[row + itemRow][col + itemCol]){

                            accumulationPointValue++;
                        }
                    }
                }
                accumulationPointMapArray[row][col] = accumulationPointValue;
            }
        }
        this.accumulationPointMap = new AccumulationPointMap(accumulationPointMapArray);

        String spaceItemName = spaceItem.getClass().getSimpleName().toLowerCase();

        System.out.println(String.format("Detecting %ss in image file: %s ", spaceItemName, image.getFileName()));
    }

    /**
     * Gets accumulation point map.
     *
     * @return the accumulation point map
     */
    public AccumulationPointMap getAccumulationPointMap() {
        return accumulationPointMap;
    }

    /**
     * Get best matches for the original array.
     *
     * @param originalArray the original array
     */
    @Override
    public void getBestMatches(int[][] originalArray){

        System.out.println("Finding the space items with a minimum probability of: " + probability + "%" );
        System.out.println("Ignoring points that lie within +/- " + duplicateAreaPoints+ " points from the target.");
        System.out.println("Maximum points for a best match: " + spaceItemArea);
        System.out.println("************** List of possible candidates **************");

        //Create a sorted accumulation point map array based on the original array.
        int [][] sortedAccumulationPointMapArray = new int[originalArray.length][];
        for(int i = 0; i<originalArray.length;i++){
            sortedAccumulationPointMapArray[i] = Arrays.copyOf(originalArray[i],originalArray[i].length);
        }

        //Flatten the array in order to be able to easily find the top N points based on the probability
        int [] vector = arrayFlatten(sortedAccumulationPointMapArray);
        final Integer[] sortedVector = ArrayUtils.toObject(vector);

        //gets the sorte map of points with their locations
        ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations =
                getSortedMapOfPointsAndLocations(sortedVector);

        //Removes any duplicates and false positives.
        duplicateRemoverAtPoint(sortedMapOfPointsAndLocations.descendingMap(), duplicateAreaPoints);

        //Prints results on screen.
        prettyPrintBestMatchesAndLocations(sortedMapOfPointsAndLocations);
    }

    /**
     * Pretty print best matches and locations.
     *
     * @param sortedMapOfPointsAndLocations the sorted map of points and locations
     */
    protected void prettyPrintBestMatchesAndLocations(ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations) {
        for (Map.Entry<Integer, AccumulationPointLocation> entry : sortedMapOfPointsAndLocations.descendingMap().entrySet()){
            Integer point = entry.getKey();
            AccumulationPointLocation location = entry.getValue();
            System.out.print("Probability: " + Math.round(((float) point * 100 / spaceItemArea )) + "% ");
            System.out.print("Points: " + point + " Locations: ");
            for(Map.Entry<Integer, Integer> locationMap : location.getLocationMap().entrySet()){
                int x = locationMap.getKey();
                int y = locationMap.getValue();

                System.out.print(" (" + x + "," + y + ")");

            }
            System.out.println();

        }
    }

    /**
     * Returns a sorted map of points and locations, given a sortedVector
     *
     * @param sortedVector the sorted vector
     * @return sorted map of points and locations.
     */
    private ConcurrentNavigableMap<Integer, AccumulationPointLocation> getSortedMapOfPointsAndLocations(Integer[] sortedVector) {
        //Sort 1D vector
        Arrays.sort(sortedVector);

        //Create  navitable set
        NavigableSet<Integer> sortedSet = new TreeSet<Integer>(Arrays.asList(sortedVector));

        //Sort the set and remove unwanted points based on the probability
        SortedSet<Integer> sortedSetHead = sortedSet.descendingSet().headSet(getProbabilityEquivalentNumber(probability));

        ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations =
                new ConcurrentSkipListMap<Integer,AccumulationPointLocation>();

        //Add valid points to the sorted map of points and location
        for (int value : sortedSetHead) {
            //System.out.println(value + searchAccumulationPoint(value));
            sortedMapOfPointsAndLocations.put(value, getAccumulationPointLocation(value));
        }
        return sortedMapOfPointsAndLocations;
    }

    /**
     * Returns an integer representing the number of points equivalent to a given the probability.
     * @param probability integer between 0 and 100
     *
     * @return instance of Integer - number of points
     */
    private Integer getProbabilityEquivalentNumber(int probability) {
        if(probability > -1 && probability <101){
            return Math.round(((float)(spaceItemArea * probability) / 100 ));
        } else {
            return 0;
        }
    }

    /**
     * Array flatten.
     * - Flattens the array (2D -> 1D)
     *
     * @param arr the arr
     * @return the int [ ] 1D array
     */
    public static int[] arrayFlatten(int[][] arr) {
        int[] vector = null;
        for (int[] anArr : arr) {
            vector = ArrayUtils.addAll(vector, anArr);
        }
        return vector;
    }

    /**
     * Gets the accumulation point location for a particular point
     *
     * @param point the point
     * @return location
     */
    @Override
    public AccumulationPointLocation getAccumulationPointLocation(int point){

        int [][] originalArray = accumulationPointMap.getAccumulationPointMapArray();

        AccumulationPointLocation location = new AccumulationPointLocation();

        for(int row = 0; row < originalArray.length ; row++) {
            for(int col = 0; col < originalArray[row].length; col++) {
                if(originalArray[row][col] == point) {
                    location.addLocation(row, col);
                }
            }
        }
        return location;
    }

    /**
     * Tries to find any 'duplicates' surrounding a maximum accumulation point, as the surroundings will also have
     * high values but they will belong to the same target.
     *
     * Adjusting the selection is important and can avoid potential duplicates, while maintaining a low number as the
     * selection points will also avoid preventing the finder to ignore targets close to the original one. This needs to
     * be adjusted based on the source file - We probably want to lower the value if the targets are too close to each other
     * or overlapping.
     *
     * A Concurrent navigable set is required - we are modifying sortedMapOfPointsAndLocations twice in the same loop.
     * This will make the duplicate remover slower, but avoid duplicating this Map in order to loop through it.
     *
     * @param sortedMapOfPointsAndLocations the sorted map of points and locations
     * @param selection the selection - Points to ignore surrounding the target.
     */
    @Override
    public void duplicateRemoverAtPoint(ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations, int selection){

        //for easy readability
        int maxX = selection;
        int maxY = selection;

        Set<Integer> duplicatePoints = new HashSet<Integer>();


        // original map of points and locations
        for (Map.Entry<Integer, AccumulationPointLocation> entry : sortedMapOfPointsAndLocations.entrySet()){
            Integer point = entry.getKey();
            AccumulationPointLocation location = entry.getValue();


            //loop through all the locations for a given point value
            for(Map.Entry<Integer, Integer> locationMap : location.getLocationMap().entrySet()){
                int originalX = locationMap.getKey();
                int originalY = locationMap.getValue();


                //loop through other points and locations map to check if that value is within the range
                for (Map.Entry<Integer, AccumulationPointLocation> entry1 : sortedMapOfPointsAndLocations.entrySet()){
                    Integer pointOther = entry1.getKey();
                    AccumulationPointLocation locationOther = entry1.getValue();

                    if(!point.equals(pointOther) && !entry.equals(entry1) && !duplicatePoints.contains(point)) {

                        /* loop through the locations for the other point values and add any duplicate points
                         * within the range to the duplicatePoints Set.
                         */

                        for(Map.Entry<Integer, Integer> locationMapOther : locationOther.getLocationMap().entrySet()){
                            int otherX = locationMapOther.getKey();
                            int otherY = locationMapOther.getValue();

                            int maxOtherX = originalX + maxX;
                            int maxOtherY = originalY + maxY;

                            int minOtherX = originalX - maxX;
                            int minOtherY = originalY - maxY;

                            if(otherX >= minOtherX && otherX <= maxOtherX && otherY >= minOtherY && otherY <= maxOtherY) {
                                duplicatePoints.add(pointOther);
                            }
                        }


                    }
                }

            }
        }
        // Remove duplicate points from the map of points and locations.
        for(Integer duplicatedPoint : duplicatePoints) {
            sortedMapOfPointsAndLocations.remove(duplicatedPoint);
        }
    }

}

