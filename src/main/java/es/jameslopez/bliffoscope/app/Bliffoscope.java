package es.jameslopez.bliffoscope.app;

import es.jameslopez.bliffoscope.reader.ImageSimpleReader;
import es.jameslopez.bliffoscope.reader.SpaceShipSimpleReader;
import es.jameslopez.bliffoscope.reader.TorpedoSimpleReader;
import es.jameslopez.bliffoscope.scanner.GenericSpaceItemScanner;
import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;
import es.jameslopez.bliffoscope.spaceitem.Image;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Class: Bliffoscope
 *
 * Main class. Very basic application that by default tries to find any
 * spaceships or torpedos using a default test data or a new image passed
 * in the command line arguments.
 *
 * Default options are 70% probability of a perfect match and ignore possible
 * duplicates within +/- 10 points from the target.
 */
public class Bliffoscope {

    /**
     * Runs the application. Also handles the command line options specified
     * as arguments.
     *
     * @param args the args
     */
    public static void main(String args[]){

        String percentage = "70";
        String points = "10";
        String imageFileName = "/TestData.blf";

        Options options = new Options();
        options.addOption("p", true, "[percentage] 0-100 Percentage/threshold of common points - Default: 70");
        options.addOption("o", true, "[points] Surrounding points to an accumulation point (target) to be ignored - Default: 10");
        options.addOption("i", true, "[fileName] Custom image file name for scan - Default: TestData.blf");

        //parse command line arguments
        CommandLineParser parser = new BasicParser();
        CommandLine cmd;
        try{
            cmd = parser.parse(options, args);
        }catch (ParseException pe){
            usage(options);
            return;
        }

        if (cmd.hasOption('p')) {
            percentage = cmd.getOptionValue('p', "70");
        }

        if (cmd.hasOption('o')) {
            points = cmd.getOptionValue('o', "10");
        }

        if (cmd.hasOption('i')) {
            imageFileName = cmd.getOptionValue('i');
        }

        //display the logo, author information, etc...
        printLogo();

        System.out.println("-- Bliffoscope anti-neutrino detection system - v1.0 (Author: James Lopez <james@jameslopez.es>)");
        System.out.println("Use: java -jar bliffoscope.jar -h to see a list with all options available.");

        //Create the image from the file
        ImageSimpleReader imageSimpleReader = new ImageSimpleReader(imageFileName,cmd.hasOption('i'));
        Image image = (Image) imageSimpleReader.getSpaceItemFromFile();

        //Create the torpedo from the file and launch the finder to show if there are any matches
        TorpedoSimpleReader torpedoSimpleReader = new TorpedoSimpleReader();
        AbstractSpaceItem torpedo = torpedoSimpleReader.getSpaceItemFromFile();
        launchFinder(image, torpedo, Integer.parseInt(percentage), Integer.parseInt(points));

        System.out.println();

        //Create the spaceship from the file and launch the finder to show if there are any matches
        SpaceShipSimpleReader spaceShipSimpleReader = new SpaceShipSimpleReader();
        AbstractSpaceItem spaceShip = spaceShipSimpleReader.getSpaceItemFromFile();
        launchFinder(image, spaceShip, Integer.parseInt(percentage), Integer.parseInt(points));

    }

    /**
     * Prints the logo of the application
     */
    private static void printLogo() {
        BliffoscopeLogoPrinter logoPrinter = new BliffoscopeLogoPrinter();
        logoPrinter.printLogoToScreen();
    }

    /**
     * Launches the finder (a generic space item scanner)
     *
     * @param image the base data image
     * @param spaceItem the space item =
     * @param percentage the percentage
     * @param points ignore points
     */
    private static void launchFinder(Image image, AbstractSpaceItem spaceItem, int percentage, int points) {
        GenericSpaceItemScanner scanner = new GenericSpaceItemScanner(percentage,points);
        scanner.findSpaceItem(spaceItem,image);
        scanner.getBestMatches(scanner.getAccumulationPointMap().getAccumulationPointMapArray());
    }

    /**
     * Display command line options available
     *
     * @param options the options
     */
    private static void usage(Options options){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "java -jar bliffoscope.jar", options );
    }

}
