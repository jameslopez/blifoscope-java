package es.jameslopez.bliffoscope.app;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Class: BliffoscopeLogoPrinter
 *
 * Basic file loader and printer, in orde to print
 * a logo at application startup.
 */
public class BliffoscopeLogoPrinter {

    /** logo file name, should be in the classpath **/
    private static final String logoFileName = "/asciiship.txt";

    /**
     * Prints the application logo
     * Scanner one liner safe only for small text files (< 1024 byes)
     */
    public void printLogoToScreen(){
        String text = null;
        InputStream in = BliffoscopeLogoPrinter.class
                .getResourceAsStream(logoFileName);
        if (in == null) {
            try {
                throw new FileNotFoundException("File not found in classpath: " + logoFileName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return ;
            }
        }
        try {
            text = new Scanner(in,"utf-8").useDelimiter("\\A").next();
        } catch (Exception e) {
            e.printStackTrace();
        }

        print(text);
    }

    protected void print(String text) {
        System.out.println(text);
    }
}
