package es.jameslopez.bliffoscope.spaceitem;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Class: AbstractSpaceItem
 *
 * Represents a space item.
 * It also counts the number of rows and columns in order to create an array with a fixed width and height.
 */
public abstract class AbstractSpaceItem {

    private boolean[][] antiNeutrinoBinaryImage;

    private int width;
    private int height;

    /**
     * Gets file name.
     *
     * @return the file name
     */
    public abstract String getFileName();

    /**
     * Instantiates a new Abstract space item.
     *
     * @param filename the filename
     */
    public AbstractSpaceItem(String filename) {
        createNeutrinoBinaryImage(filename);
    }

    /**
     * Instantiates a new Abstract space item.
     */
    public AbstractSpaceItem(){

    }

    /**
     * Create neutrino binary image.
     *
     * - It creates a 2D 'squared matrix' of booleans based on the maximum width and height.
     *
     * @param filename the filename
     */
    protected void createNeutrinoBinaryImage(String filename) {
        int count = countLinesAndColumns(filename);

        this.height = count; //lines / rows
        this.width = count; // columns

        antiNeutrinoBinaryImage = new boolean[width][height];
    }

    /**
     * Get anti neutrino binary image.
     *
     * @return the boolean [ ] [ ]
     */
    public boolean[][] getAntiNeutrinoBinaryImage() {
        return antiNeutrinoBinaryImage.clone();
    }

    /**
     * Will actually replicate the file and show the 2D Array as the initial file.
     *
     * @return String representation of the 2D matrix using the original characters.
     */
    @Override
    public String toString() {
        StringBuilder antiNeutrinoString = new StringBuilder();
        for(int row = 0; row < antiNeutrinoBinaryImage.length ; row++) {
            for(int col = 0; col < antiNeutrinoBinaryImage[row].length; col++) {
                antiNeutrinoString.append(antiNeutrinoBinaryImage[row][col] ? '+' : ' ');
            }
            antiNeutrinoString.append(System.getProperty("line.separator"));
        }
        return antiNeutrinoString.toString();
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Counts the number of rows and columns
     *
     * @param filename the file name
     * @return maximum number of rows or columns
     */
    private int countLinesAndColumns(String filename) {
        int lineCount = 0;
        int maxCharsInLine = 0;

        InputStream is = getInputStream(filename);
        try {
            byte[] c = new byte[1024];
            int readChars;
            boolean endsWithoutNewLine = false;
            while ((readChars = is.read(c)) != -1) {
                for (int i = 0, j = 0; i < readChars; ++i, ++j) {
                    if (c[i] == '\n') {
                        ++lineCount;
                        maxCharsInLine = (j > maxCharsInLine) ? j : maxCharsInLine;
                        j=0;
                    }
                }
                endsWithoutNewLine = (c[readChars - 1] != '\n');
            }
            if(endsWithoutNewLine) {
                ++lineCount;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return (lineCount > maxCharsInLine) ? lineCount : maxCharsInLine;
    }

    /**
     * Gets input stream.
     *
     * @param filename the filename
     * @return the input stream
     */
    protected InputStream getInputStream(String filename) {
        InputStream is;

        //Safe use of getResource as no relative path would be used - but we should take this into account.
        InputStream in = this.getClass().getResourceAsStream(filename);
        if (in == null) {
            try {
                throw new FileNotFoundException("File not found in classpath: " + filename);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        is = new BufferedInputStream(in);
        return is;
    }
}
