package es.jameslopez.bliffoscope.spaceitem;

/**
 * Class: Torpedo
 *
 * Represents a torpedo
 */
public class Torpedo extends AbstractSpaceItem {

    private static final String fileName = "/SlimeTorpedo.blf";

    /**
     * Instantiates a new Torpedo.
     */
    public Torpedo() {
        super(fileName);
    }

    @Override
    public String getFileName() {
        return fileName;
    }
}
