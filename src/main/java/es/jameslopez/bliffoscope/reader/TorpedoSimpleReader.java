package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;
import es.jameslopez.bliffoscope.spaceitem.Torpedo;

/**
 * Class: TorpedoSimpleReader
 *
 * Reads a torpedo file
 */
public class TorpedoSimpleReader extends BlfSimpleReader {

    @Override
    public AbstractSpaceItem getSpaceItem() {
        return new Torpedo();
    }
}
