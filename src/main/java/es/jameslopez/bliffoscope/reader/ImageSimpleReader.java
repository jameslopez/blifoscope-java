package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;
import es.jameslopez.bliffoscope.spaceitem.Image;

/**
 * Class: ImageSimpleReader
 *
 * Reads an image file
 */
public class ImageSimpleReader extends BlfSimpleReader {

    String fileName;
    boolean readFromFile;

    /**
     * Instantiates a new Image simple reader.
     *
     * @param fileName the file name
     * @param readFromFile the read from file boolean (if false, it will load the file from classpath)
     */
    public ImageSimpleReader(String fileName, boolean readFromFile) {
        this.fileName = fileName;
        this.readFromFile = readFromFile;
    }

    @Override
    public AbstractSpaceItem getSpaceItem() {
        return new Image(fileName, readFromFile);
    }
}
