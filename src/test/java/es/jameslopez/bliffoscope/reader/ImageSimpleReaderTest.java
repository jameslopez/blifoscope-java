package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.Image;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Class: TorpedoSimpleReaderTest
 */
public class ImageSimpleReaderTest {

    @Test
    public void testReadImageDataFromClasspath(){

        ImageSimpleReader imageSimpleReader = new ImageSimpleReader("/TestData.blf",false);
        Image image = (Image) imageSimpleReader.getSpaceItemFromFile();
        int plusCount = StringUtils.countMatches(image.toString(), "+");
        assertEquals(2127,plusCount);

        int spaceCount = StringUtils.countMatches(image.toString(), " ");
        assertEquals(8074,spaceCount);
    }

    @Test
    public void testReadImageDataFromFile(){

        ImageSimpleReader imageSimpleReader = new ImageSimpleReader("src/test/resources/TestData.blf",true);
        Image image = (Image) imageSimpleReader.getSpaceItemFromFile();
        int plusCount = StringUtils.countMatches(image.toString(), "+");
        assertEquals(2127,plusCount);

        int spaceCount = StringUtils.countMatches(image.toString(), " ");
        assertEquals(8074,spaceCount);
    }


}
