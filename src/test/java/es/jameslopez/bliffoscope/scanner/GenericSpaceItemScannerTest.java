package es.jameslopez.bliffoscope.scanner;

import es.jameslopez.bliffoscope.reader.ImageSimpleReader;
import es.jameslopez.bliffoscope.reader.TorpedoSimpleReader;
import es.jameslopez.bliffoscope.spaceitem.Image;
import es.jameslopez.bliffoscope.spaceitem.Torpedo;
import junit.framework.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Class: GenericSpaceItemScannerTest
 */
public class GenericSpaceItemScannerTest {
    @Test
    public void testFindSpaceItem() throws Exception {
        TorpedoSimpleReader torpedoSimpleReader = new TorpedoSimpleReader();
        Torpedo torpedo1 = (Torpedo) torpedoSimpleReader.getSpaceItemFromFile();

        ImageSimpleReader imageSimpleReader = new ImageSimpleReader("/TestData.blf", false);

        Image image = (Image) imageSimpleReader.getSpaceItemFromFile();
        final int spaceItemArea = torpedo1.getWidth() * torpedo1.getHeight();

        final boolean[] enteredFor = {false};

        GenericSpaceItemScanner scanner = new GenericSpaceItemScanner(80,2){
            @Override
            public void prettyPrintBestMatchesAndLocations(ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations) {
                for (Map.Entry<Integer, AccumulationPointLocation> entry : sortedMapOfPointsAndLocations.descendingMap().entrySet()){
                    Integer point = entry.getKey();
                    AccumulationPointLocation location = entry.getValue();

                    assertEquals(Integer.valueOf(137),point);

                    System.out.print("Probability: " + Math.round(((float) point * 100 / spaceItemArea )) + "% ");
                    System.out.print("Points: " + point + " Locations: ");
                    for(Map.Entry<Integer, Integer> locationMap : location.getLocationMap().entrySet()){
                        int x = locationMap.getKey();
                        int y = locationMap.getValue();

                        enteredFor[0] = true;

                        assertEquals(0,x);
                        assertEquals(0,y);

                        System.out.print(" (" + x + "," + y + ")");

                    }
                }
            }
        };

        scanner.findSpaceItem(torpedo1, image);
        scanner.getBestMatches(scanner.getAccumulationPointMap().getAccumulationPointMapArray());
        assertTrue(enteredFor[0]);

    }

    @Test
    public void testRemoveDuplicatesWithOneDuplicate(){
        ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations = new ConcurrentSkipListMap<Integer, AccumulationPointLocation>();

        AccumulationPointLocation location1 = new AccumulationPointLocation();
        location1.addLocation(1,1);


        AccumulationPointLocation location2 = new AccumulationPointLocation();
        location2.addLocation(5, 4);

        sortedMapOfPointsAndLocations.put(1,location1);
        sortedMapOfPointsAndLocations.put(2,location2);

        SpaceItemScanner scanner = new GenericSpaceItemScanner(50,2);
        scanner.duplicateRemoverAtPoint(sortedMapOfPointsAndLocations,4);

        boolean enteredFor = false;

        for (Map.Entry<Integer, AccumulationPointLocation> entry : sortedMapOfPointsAndLocations.entrySet()){
            Integer point = entry.getKey();
            AccumulationPointLocation location = entry.getValue();

            System.out.print(point + " Locations: ");
            for(Map.Entry<Integer, Integer> locationMap : location.getLocationMap().entrySet()){
                int x = locationMap.getKey();
                int y = locationMap.getValue();

                assertEquals(1,x);
                assertEquals(1,y);

                enteredFor = true;

                System.out.print(" [" + x + "][" + y + "]");

            }
            System.out.println();
        }

        assertTrue(enteredFor);
    }

    @Test
    public void testRemoveDuplicatesWithNoDuplicates(){
        ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations = new ConcurrentSkipListMap<Integer, AccumulationPointLocation>();

        AccumulationPointLocation location1 = new AccumulationPointLocation();
        location1.addLocation(1,1);


        AccumulationPointLocation location2 = new AccumulationPointLocation();
        location2.addLocation(5, 5);

        sortedMapOfPointsAndLocations.put(1,location1);
        sortedMapOfPointsAndLocations.put(2,location2);

        SpaceItemScanner scanner = new GenericSpaceItemScanner(50,2);
        scanner.duplicateRemoverAtPoint(sortedMapOfPointsAndLocations,2);

        int enteredCount = 0;
        int i = 1;

        for (Map.Entry<Integer, AccumulationPointLocation> entry : sortedMapOfPointsAndLocations.entrySet()){
            Integer point = entry.getKey();
            AccumulationPointLocation location = entry.getValue();

            System.out.print(point + " Locations: ");
            for(Map.Entry<Integer, Integer> locationMap : location.getLocationMap().entrySet()){
                int x = locationMap.getKey();
                int y = locationMap.getValue();

                assertEquals(i,x);
                assertEquals(i,y);

                i+=4;

                enteredCount++;

                System.out.print(" [" + x + "][" + y + "]");

            }
            System.out.println();
        }
        Assert.assertEquals(2,enteredCount);

    }
}
